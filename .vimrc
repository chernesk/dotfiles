" Vim: curl -sfLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
" NeoVim: curl -sfLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
" call plug#begin('~/.vim/plugged')

if empty(glob('~/.vim/autoload/plug.vim'))
  silent execute "!curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
  autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin()
    " Plug 'mikewest/vimroom'
    " Plug 'ajh17/VimCompletesMe'
    " Plug 'altercation/vim-colors-solarized'
    " Plug 'c-brenn/phoenix.vim'
    " Plug 'chase/vim-ansible-yaml'
    " Plug 'chriskempson/base16-vim'
    " Plug 'danchoi/ri.vim'
    " Plug 'derekwyatt/vim-scala'
    " Plug 'dkprice/vim-easygrep'
    " Plug 'elixir-lang/vim-elixir'
    " Plug 'elzr/vim-json'
    Plug 'godlygeek/tabular'
    " Plug 'idanarye/vim-merginal'
    " Plug 'imeos/vim-colors-solstice'
    " Plug 'kana/vim-textobj-user'
    " Plug 'kien/rainbow_parentheses.vim'
    " Plug 'klen/python-mode', { 'for': 'python' }
    " Plug 'ludovicchabant/vim-lawrencium'
    " Plug 'majutsushi/tagbar'
    " Plug 'mattn/webapi-vim'
    " Plug 'mhinz/vim-signify'
    " Plug 'nelstrom/vim-textobj-rubyblock'
    " Plug 'palmer-eldritch/ri.vim'
    " Plug 'pangloss/vim-javascript'
    " Plug 'scrooloose/syntastic'
    " Plug 'chrisbra/Recover.vim'
    " Plug 'ngmy/vim-rubocop'
    " Plug 'skywind3000/asyncrun.vim'
    " Plug 'thinca/vim-visualstar'
    " Plug 'tpope/vim-projectionist'
    " Plug 'trusktr/seti.vim'
    " Plug 'vim-scripts/Colour-Sampler-Pack'
    " Plug 'vim-scripts/CycleColor'
    " Plug 'vim-scripts/L9'
    " Plug 'vim-scripts/ScrollColors'
    " Plug 'vim-scripts/ZoomWin'
    " Plug 'vim-scripts/auto_mkdir'
    " Plug 'vim-scripts/groovy.vim'
    " Plug 'vim-scripts/vcscommand.vim'
    " Plug 'xolox/vim-easytags'
    Plug 'dense-analysis/ale'
    "Plug 'godlygeek/csapprox'
    " Plug 'ConradIrwin/vim-bracketed-paste'
    " Plug 'bogado/file-line'
    Plug 'kopischke/vim-fetch' 			"  This opens rspec like line numbers
    Plug 'farmergreg/vim-lastplace' 		"  This opens a file at the line number you last edited
    " Plug 'ctrlpvim/ctrlp.vim'
    " Plug 'd11wtq/ctrlp_bdelete.vim'
    Plug 'ervandew/supertab'
    Plug 'flazz/vim-colorschemes'
    Plug 'janko-m/vim-test'
    " Plug 'jiangmiao/auto-pairs'
    Plug 'junegunn/goyo.vim'
    Plug 'kchmck/vim-coffee-script'
    Plug 'ludovicchabant/vim-gutentags'
    Plug 'michaeljsmith/vim-indent-object'
    Plug 'powerman/vim-plugin-AnsiEsc'
    Plug 'rking/ag.vim'
    Plug 'rondale-sc/vim-spacejam'
    " Plug 'scrooloose/nerdtree' ", { 'on': 'NERDTreeToggle' }
    Plug 'sickill/vim-pasta'
    Plug 'slim-template/vim-slim'
    " Plug 'tacahiroy/ctrlp-funky'
    " Plug 'thaerkh/vim-workspace'
    " Plug 'tmhedberg/matchit'
    " Plug 'tomtom/tcomment_vim'
    Plug 'tpope/vim-commentary'
    Plug 'tpope/vim-abolish'
    Plug 'tpope/vim-cucumber'
    Plug 'tpope/vim-dispatch'
    Plug 'tpope/vim-endwise'
    Plug 'tpope/vim-fugitive'
    Plug 'tpope/vim-git'
    Plug 'tpope/vim-haml'
    Plug 'tpope/vim-markdown'
    Plug 'tpope/vim-repeat'
    Plug 'tpope/vim-surround'
    Plug 'tpope/vim-unimpaired'
    Plug 'tpope/vim-vinegar'
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
    Plug 'vim-scripts/ruby-matchit'
    Plug 'xolox/vim-misc'
    " Ruby
    Plug 'tpope/vim-rbenv'
    Plug 'tpope/vim-bundler'
    Plug 'tpope/vim-eunuch'
    " Plug 'zxiest/vim-ruby'
    Plug 'tpope/vim-rails'
    " Plug 'vim-ruby/vim-ruby'
    Plug 'rlue/vim-fold-rspec'
    Plug 'sheerun/vim-polyglot'

    Plug 'MattesGroeger/vim-bookmarks'

    Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
    Plug 'junegunn/fzf.vim'

    Plug 'vimwiki/vimwiki'

    Plug 'mattn/calendar-vim'
    " Plug 'sedm0784/vim-you-autocorrect'

    Plug 'mbbill/undotree'
    Plug 'fvictorio/vim-extract-variable'

    Plug 'yggdroot/indentline'
    Plug 'jeetsukumaran/vim-indentwise'

    " Plug 'neoclide/coc.nvim', {'branch': 'release'}

call plug#end()
silent! call ctrlp_bdelete#init()

" Vim Bookmarks
let g:bookmark_highlight_lines = 1

au FileType qf call AdjustWindowHeight(3, 30)
function! AdjustWindowHeight(minheight, maxheight)
  exe max([min([line("$"), a:maxheight]), a:minheight]) . "wincmd _"
endfunction

let g:dispatch_handlers = ['headless']

let test#strategy = "dispatch"
" " let test#strategy = "async"

let g:gutentags_cache_dir = '~/.tags_cache'

" ALE
let g:ale_sign_column_always = 0
let g:ale_lint_on_enter = 0
let g:ale_lint_on_text_changed = 'never'
let g:ale_lint_on_save = 1
highlight ALEWarning ctermbg=DarkGray
hi link ALEErrorSign    Error
hi link ALEWarningSign  Warning

" " Workspace
nnoremap <leader>s :ToggleWorkspace<CR>
set nocp
filetype plugin on
" " bring in the bundles for mac and windows
" let mapleader = ','
autocmd Filetype gitcommit setlocal spell textwidth=72

if has("vim")
  set term=screen-256color
endif

set t_Co=256
set background=dark
silent! colorscheme lucius

let g:solarized_termcolors=&t_Co
let g:solarized_termtrans=1

" Remove trailing whitespace
autocmd BufWritePre * :%s/\s\+$//e
"
" set pastetoggle=<leader>p
"
" " if !empty($MY_RUBY_HOME)
" "    let g:ruby_path = join(split(glob($MY_RUBY_HOME.'/lib/ruby/*.*')."\n".glob($MY_RUBY_HOME.'/lib/rubysite_ruby/*'),"\n"),',')
" " endif
" "
" " ruby path if you are using rbenv
" let g:ruby_path = system('echo $HOME/.rbenv/shims')
"
" " let g:loaded_netrw       = 1
" " let g:loaded_netrwPlugin = 1
"
" autocmd WinEnter * call s:CloseIfOnlyNerdTreeLeft()
"
" Close all open buffers on entering a window if the only
" buffer that's left is the NERDTree buffer
function! s:CloseIfOnlyNerdTreeLeft()
  if exists("t:NERDTreeBufName")
    if bufwinnr(t:NERDTreeBufName) != -1
      if winnr("$") == 1
        q
      endif
    endif
  endif
endfunction
"
" The Silver Searcher Config
nmap g/ :Ag!<space>
nmap g* :Ag! -w <C-R><C-W><space>
nmap ga :AgAdd!<space>
nmap gn :cnext<CR>
" nmap gp :cprev<CR>
nmap gq :ccl<CR>
nmap gl :cwindow<CR>

nmap f :NERDTreeFind<CR>
nmap gt :NERDTreeToggle<CR>
nmap g :NERDTree \| NERDTreeToggle \| NERDTreeFind<CR>
"
" Tagbar Configuration
let g:tagbar_autofocus = 1
map <Leader>rt :!ctags --extra=+f -R *<CR><CR>
map <Leader>. :TagbarToggle<CR>
"
" " Markdown Syntax Highlighting
" augroup mkd
"   autocmd BufNewFile,BufRead *.mkd      set ai formatoptions=tcroqn2 comments=n:> filetype=markdown
"   autocmd BufNewFile,BufRead *.md       set ai formatoptions=tcroqn2 comments=n:> filetype=markdown
"   autocmd BufNewFile,BufRead *.markdown set ai formatoptions=tcroqn2 comments=n:> filetype=markdown
" augroup END
"
" au! BufRead,BufNewFile *.json set filetype=json
"
" " Markdown Preview
" map <buffer> <Leader>mp :Mm<CR>
"
" " NERDTree for project drawer
" let NERDTreeHijackNetrw = 0
"
"
" " Tabular
" function! CustomTabularPatterns()
"   if exists('g:tabular_loaded')
"     AddTabularPattern! symbols         / :/l0
"     AddTabularPattern! hash            /^[^>]*\zs=>/
"     AddTabularPattern! chunks          / \S\+/l0
"     AddTabularPattern! assignment      / = /l0
"     AddTabularPattern! comma           /^[^,]*,/l1
"     AddTabularPattern! colon           /:\zs /l0
"     AddTabularPattern! options_hashes  /:\w\+ =>/
"   endif
" endfunction
"
" autocmd VimEnter * call CustomTabularPatterns()
"
" " shortcut to align text with Tabular
" map <Leader>a :Tabularize<space>
"
" " Zoomwin
" map <Leader>z :ZoomWin<CR>
"
" Unimpaired
" Bubble single lines
nmap <C-Up> [e
nmap <C-Down> ]e

" Bubble multiple lines
vmap <C-Up> [egv
vmap <C-Down> ]egv
"
" " Syntastic
" " let g:syntastic_auto_loc_list = 2
" " let g:syntastic_check_on_open = 1
" " let g:syntastic_check_on_wq = 0
" " let g:syntastic_enable_balloons = 0
" " let g:syntastic_error_symbol = '✗'
" " let g:syntastic_ignore_files = ['\.min\.js$', '\.min\.css$']
" " let g:syntastic_loc_list_height = 5
" " let g:syntastic_warning_symbol = '✗'
" " let g:syntastic_style_error_symbol = '∆'
" " let g:syntastic_style_warning_symbol = '∆'
" " let g:syntastic_enable_signs=1
" " let g:syntastic_ruby_checkers          = ['rubocop', 'mri']
" " let g:syntastic_always_populate_loc_list = 1
"
" " let g:syntastic_mode_map = { 'mode': 'active',
" "                            \ 'active_filetypes': [],
" "                            \ 'passive_filetypes': ['haml','scss','sass'] }
" " let g:syntastic_quiet_messages = {'level': 'warnings'}
" " syntastic is too slow for haml and sass
" " Vim Rails
" map <Leader>oc :Rcontroller<Space>
" map <Leader>ov :Rview<Space>
" map <Leader>om :Rmodel<Space>
" map <Leader>oh :Rhelper<Space>
" map <Leader>oj :Rjavascript<Space>
" map <Leader>os :Rstylesheet<Space>
" map <Leader>oi :Rintegration<Space>
"
" vim-surround
" # to surround with ruby string interpolation
let g:surround_35 = "#{\r}"
" - to surround with no-output erb tag
let g:surround_45 = "<% \r %>"
" = to surround with output erb tag
let g:surround_61 = "<%= \r %>"

" set clipboard=unnamed
set tags=./tags,tags,../tags

set mouse=
let g:airline_powerline_fonts = 1
" let g:airline_theme             = 'powerlineish'
let g:airline_theme             = 'solarized'
let g:airline#extensions#branch#enabled = 1
" let g:airline#extensions#syntastic#enabled = 1
"let g:airline_detect_modified=1

" Trailing whitespace
" function! TrimWhiteSpace()
"     %s/\s\+$//e
" endfunction

" nnoremap <silent> <Leader>rts :call TrimWhiteSpace()<CR>
filetype plugin on

" JSON
set conceallevel=0
"
" " PyMode
" let g:pymode_breakpoint = 0
" let g:pymode_folding = 0
"
" " Merginal
" nmap <Leader>m :Merginal<CR>
"
" fun! Html2haml()
"   %!html2haml -r
"   save %:r.haml
"   setf haml
"   !git rm %:r.erb
" endfun
"
" let html_use_css = 1 " Use stylesheet instead of inline style
" let html_number_lines = 0 " don't show line numbers
" let html_no_pre = 1 " don't wrap lines in <pre>
"
" function! OpenHtml(line1, line2)
"   exec a:line1.','.a:line2.'TOhtml'
"   %s/monospace/Monaco/g
"   %s/bold/normal/g
"   save! /tmp/__OpenHtml.html
"   !open %
"   q
" endfunction
" command! -range=% OpenHtml :call OpenHtml(<line1>,<line2>)
"
" " Tidy an HTML/XML file inline
" command! Tidy :%! tidy -indent -quiet -wrap 100
"
" " Align all colon-separated content (CSS rules) in a file
" command! AlignColons execute 'g/:/Tabularize colon' | noh
"
" " via: http://rails-bestpractices.com/posts/60-remove-trailing-whitespace
" " Strip trailing whitespace
" function! <SID>StripTrailingWhitespaces()
"     " Preparation: save last search, and cursor position.
"     let _s=@/
"     let l = line(".")
"     let c = col(".")
"     " Do the business:
"     %s/\s\+$//e
"     " Clean up: restore previous search history, and cursor position
"     let @/=_s
"     call cursor(l, c)
" endfunction
" command! StripTrailingWhitespaces call <SID>StripTrailingWhitespaces()
"
" required for several plugins
  set nocompatible

" enable syntax highlighting
  syntax on

" default color scheme
  set background=dark

" don't wrap long lines
  set nowrap

" show commands as we type them
  set showcmd

" highlight matching brackets
  set showmatch

" scroll the window when we get near the edge
  set scrolloff=4 sidescrolloff=10

" use 2 spaces for tabs
  set expandtab tabstop=2 softtabstop=2 shiftwidth=2

" enable line numbers, and don't make them any wider than necessary
  set number numberwidth=2

" show the first match as search strings are typed
  set incsearch

" highlight the search matches
  set hlsearch

" searching is case insensitive when all lowercase
  set ignorecase smartcase

" assume the /g flag on substitutions to replace all matches in a line
"  set gdefault

" set temporary directory (don't litter local dir with swp/tmp files)
  set directory=/tmp/

" pick up external file modifications
  set autoread

" don't abandon buffers when unloading
  set hidden

" match indentation of previous line
  set autoindent

" perform autoindenting based on filetype plugin
  filetype plugin indent on

" don't blink the cursor
  set guicursor=a:blinkon0

" show current line info (current/total)
  set ruler rulerformat=%=%l/%L

" show status line
  set laststatus=2

" " augment status line
"   function! ETry(function, ...)
"     if exists('*'.a:function)
"       return call(a:function, a:000)
"     else
"       return ''
"     endif
"   endfunction
"   set statusline=[%n]\ %<%.99f\ %h%w%m%r%{ETry('CapsLockStatusline')}%y%{ETry('rails#statusline')}%{ETry('fugitive#statusline')}%#ErrorMsg#%*%=%-16(\ %l,%c-%v\ %)%P
"
" " When lines are cropped at the screen bottom, show as much as possible
"   set display=lastline
"
" flip the default split directions to sane ones
  set splitright
  set splitbelow

" don't beep for errors
  set visualbell

" make backspace work in insert mode
  set backspace=indent,eol,start

" highlight trailing whitespace
  set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+
  set list

" use tab-complete to see a list of possiblities when entering commands
  set wildmode=list:longest,full

" allow lots of tabs
  set tabpagemax=20

" " remember last position in file
"   au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal g'\"" | endif
"
" Thorfile, Rakefile, Vagrantfile, and Gemfile are Ruby
  au BufRead,BufNewFile {Gemfile,Rakefile,Vagrantfile,Thorfile,config.ru} set ft=ruby


" " easy wrap toggling
"   nmap <Leader>w :set wrap!<cr>
"   nmap <Leader>W :set nowrap<cr>
"
" " go to the alternate file (previous buffer) with g-enter
" "  nmap g
"
" shortcuts for frequenly used files
  nmap gs :e db/schema.rb<cr>
  nmap gr :e config/routes.rb<cr>
  nmap gm :e Gemfile<cr>

" shortcut for =>
  imap <C-l> <Space>=><Space>

  abbrev hte the

" clean up trailing whitespace
  " map <Leader>c :StripTrailingWhitespaces<cr>

" compress excess whitespace on current line
  map <Leader>e :s/\v(\S+)\s+/\1 /<cr>:nohl<cr>

" delete all buffers
  map <Leader>d :bufdo bd<cr>

" map spacebar to clear search highlight
  nnoremap <Leader><space> :noh<cr>

" buffer resizing mappings (shift + arrow key)
  nnoremap <S-Up> <c-w>+
  nnoremap <S-Down> <c-w>-
  nnoremap <S-Left> <c-w><
  nnoremap <S-Right> <c-w>>

" " reindent the entire file
"   map <Leader>I gg=G``<cr>
"
" " insert the path of currently edited file into a command
" " Command mode: Ctrl-P
"   cmap <C-S-P> <C-R>=expand("%:p:h") . "/" <cr>
"
"
" set nolist
" let g:NERDTreeMinimalUI=1
let g:fuf_file_exclude = '\v\~$|\.o$|\.exe$|\.bak$|\.swp|\.svn|\.class$'
" set wrap
" set linebreak
" set nolist  " list disables linebreak
" " let g:ctrlp_max_files=0
" " let g:ctrlp_arg_map = 1
" " let g:ctrlp_match_window = 'bottom,order:btt,min:1,max:30,results:100'
" autocmd QuickFixCmdPost *grep* cwindow
" let g:syntastic_javascript_checkers = ['jsl']
"
" command! -nargs=0 -bar Qargs execute 'args' QuickfixFilenames()
" " populate the argument list with each of the files named in the quickfix list
" function! QuickfixFilenames()
"   let buffer_numbers = {}
"   for quickfix_item in getqflist()
"     let buffer_numbers[quickfix_item['bufnr']] = bufname(quickfix_item['bufnr'])
"   endfor
"   return join(map(values(buffer_numbers), 'fnameescape(v:val)'))
" endfunction
nmap <Leader>gg :Gstatus<CR>
nmap <Leader>gw :Gwrite<CR>
nmap <Leader>gb :Gblame<CR>
nmap <Leader>gs :w<CR>:Git add %<CR>
nmap <Leader>gp :Git push<<CR>
"
let loaded_matchparen = 0
"
" function! TwiddleCase(str)
"   if a:str ==# toupper(a:str)
"     let result = tolower(a:str)
"   elseif a:str ==# tolower(a:str)
"     let result = substitute(a:str,'\(\<\w\+\>\)', '\u\1', 'g')
"   else
"     let result = toupper(a:str)
"   endif
"   return result
" endfunction
" vnoremap ~ y:call setreg('', TwiddleCase(@"), getregtype(''))<CR>gv""Pgv
"
" autocmd BufWritePost * if &diff == 1 | diffupdate | endif
" let g:vim_json_syntax_conceal=0
" set tags=./tags;
" let g:easytags_dynamic_files = 1
" let g:easytags_auto_highlight = 0
" let g:easytags_async = 1
" nmap <leader>rh :%s/\v:(\w+) \=\>/\1:/g<cr>
"
" if executable('ag')
"   " Use ag over grep
"   set grepprg=ag\ --nogroup\ --nocolor
"
"   " Use ag in CtrlP for listing files. Lightning fast and respects .gitignore
"   let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
"
"   " ag is fast enough that CtrlP doesn't need to cache
"   let g:ctrlp_use_caching = 0
" endif

" The Silver Searcher
if executable('ag')
  " Use ag over grep
  set grepprg=ag\ --nogroup\ --nocolor
endif
nnoremap K :grep! "\b<C-R><C-W>\b"<CR>:cw<CR>



" Supertab
inoremap <expr> <Space> pumvisible() ? "\<C-y>" : " "
"
" " diff coloscheme changes
" " if &diff
" "   colorscheme jellybean
" " endif
" " au FilterWritePre * if &diff | colorscheme jellybean | endif
"
" " Ruby hash syntax conversion
" nnoremap <Leader>hc :%s/:\([^ ]*\)\(\s*\)=>/\1:/g<return>
" autocmd FileType ruby let b:vcm_tab_complete = "tags"
" autocmd Syntax * syn match ExtraWhitespace /\s\+$\| \+\ze\\t/
" set ttyfast
" au FileWritePost * :redraw!
" au TermResponse * :redraw!
" au TextChanged * :redraw!
" au QuickFixCmdPre * :redraw!
" au QuickFixCmdPost * :redraw!
"
" nmap <Leader>di a[disabled='disabled']<ESC>
"
" function! UnMinify()
"     %s/{\ze[^\r\n]/{\r/g
"     %s/){/) {/g
"     %s/};\?\ze[^\r\n]/\0\r/g
"     %s/;\ze[^\r\n]/;\r/g
"     %s/[^\s]\zs[=&|]\+\ze[^\s]/ \0 /g
"     normal ggVG=
" endfunction
"
" if has("gui_macvim")
"   set clipboard=unnamed
" endif
"
" let g:ale_statusline_format = ['⨉ %d', '⚠ %d', '⬥ ok']
"
nmap <silent> <leader>n :TestNearest<CR>
nmap <silent> <leader>T :TestFile<CR>
nmap <silent> <leader>a :TestSuite<CR>
nmap <silent> <leader>l :TestLast<CR>
nmap <silent> <leader>rs :! bundle exec rspec spec<CR>
" nmap <silent> <leader>g :TestVisit<CR>

set encoding=utf-8
nnoremap <tab> <c-w>
nnoremap <tab><tab> <c-w><c-w>

" Autopairs config
let g:AutoPairs =  {'(':')', '[':']', '{':'}','"':'"', '`':'`'}

nmap <leader>h :%!html2haml --erb 2> /dev/null<CR>:set ft=haml<CR>
vmap <leader>h :!html2haml --erb 2> /dev/null<CR>
nmap <leader><leader> :only<cr>
nmap , \

autocmd QuickFixCmdPost *grep* cwindow
set viminfo=""
set viminfo=<800,'10,/50,:5000,h,f0,n~/.viminfo
"set viminfo=%,<800,'10,/50,:5000,h,f0,n~/.viminfo
"           | |    |   |   |    | |  + viminfo file path
"           | |    |   |   |    | + file marks 0-9,A-Z 0=NOT stored
"           | |    |   |   |    + disable 'hlsearch' loading viminfo
"           | |    |   |   + command-line history saved
"           | |    |   + search history saved
"           | |    + files marks saved
"           | + lines saved each register (old name for <, vi6.2)
"           + save/restore buffer list
augroup quickfix
	autocmd!
	autocmd FileType qf setlocal wrap
augroup END

set timeoutlen=1000 ttimeoutlen=0
nmap <leader>bp :Ag! 'binding.pry'<CR>
"%bd|e#
" " " ctrl-p
" nmap <Leader>t :CtrlP<CR>
" nmap <Leader>m :CtrlPBufTag<CR>
" nmap <Leader>mm :CtrlPBufTagAll<CR>
" nmap <Leader>y :CtrlPBufTagAll<CR>
" set wildignore+=*.class
" "
" " ctrlp-funky
" let g:ctrlp_extensions = ['funky']
" nmap <Leader>f :CtrlPFunky<CR>

" " ctrl-p
nmap <Leader>t :Files<CR>
nmap <Leader>m :Buffers<CR>
nmap <Leader>b :Buffers<CR>
"
" ctrlp-funky
let g:ctrlp_extensions = ['funky']
nmap <Leader>f :BTags<CR>

au Filetype vimwiki Goyo 100
" | set breakindentopt=shift:2,min:40,sbr| set breakindent

function! s:goyo_enter()
  " if executable('tmux') && strlen($TMUX)
  "   silent !tmux set status off
  "   silent !tmux list-panes -F '\#F' | grep -q Z || tmux resize-pane -Z
  " endif
  " set noshowmode
  " set noshowcmd
  " set scrolloff=999
  set wrap
  set linebreak
  set spell
  EnableAutocorrect
  " setlocal comments=:# commentstring=#\ %s
  " setlocal formatoptions+=tl
  " setlocal formatoptions-=c formatoptions-=r formatoptions-=o formatoptions-=q formatoptions+=n
  " setlocal formatlistpat=\\\|^\\s*[-*+]\\s\\+
endfunction
autocmd! User GoyoEnter nested call <SID>goyo_enter()

set timeoutlen=1000 ttimeoutlen=0
" function! s:setFormatOptions()
"   setlocal comments=:# commentstring=#\ %s
"   setlocal formatoptions+=tl
"   setlocal formatoptions-=c formatoptions-=r formatoptions-=o formatoptions-=q formatoptions+=n
"   setlocal formatlistpat=\\\|^\\s*[-*+]\\s\\+
" endfunction

let g:fold_rspec_foldclose = 'all'
let g:fold_rspec_foldcolumn = 4
let g:fold_rspec_foldenable = 0

if has("autocmd")
  augroup vimwikigroup
    autocmd!
    autocmd BufNewFile */diary/** 0r ~/vimwiki/template.wiki
    autocmd BufNewFile */devotions/** 0r ~/vimwiki/journaling-template.wiki
    " au FileType vimwiki call <SID>setFormatOptions()
    autocmd BufRead,BufNewFile diary.wiki VimwikiDiaryGenerateLinks
  augroup END
endif

let g:vimwiki_use_calendar = 1
nmap <Leader>s :term bundle exec rails s<cr>
nmap <Leader>c :term bundle exec rails c<cr>
nmap <Leader>S :sbuffer rails s<cr>
set pastetoggle=<leader>p

nmap <leader>k i* (<Esc>i<C-R>=strftime('%I:%M%p')<Esc>i) - <Esc>
let g:fzf_preview_window = ''
nmap <Leader>sp screenshot_and_save_page<Esc>:w

nmap <F3> i<C-R>=strftime("%Y-%m-%d %a %I:%M %p")<CR><Esc>
imap <F3> <C-R>=strftime("%Y-%m-%d %a %I:%M %p")<CR>

" https://blog.semanticart.com/2017/11/14/rails-route-completion-with-fzf-in-vim/
function! s:parse_route(selected)
  let l:squished = substitute(join(a:selected), '^\s\+', '', '')
  return split(l:squished)[0] . '_path'
endfunction

inoremap <expr> <c-x><c-r> fzf#complete({
  \ 'source':  'rake routes',
  \ 'reducer': '<sid>parse_route'})
autocmd VimResized * if exists('#goyo') | exe "normal \<c-w>=" | endif
let g:vim_you_autocorrect_disable_highlighting = 1
